<?php 

class HomeController extends Controller
{	
	function index($data = array())
	{
		$this->load->library('Form');
		$data['default1'] = isset($data['default1']) ? $data['default1'] : 'six, negative seven hundred twenty nine, one million one hundred one';
		$data['default2'] = isset($data['default2']) ? $data['default2'] : '5, 7, 11';
		$data['default3'] = isset($data['default3']) ? $data['default3'] : '9';
		$data['default4'] = isset($data['default4']) ? $data['default4'] : '7';
		$data['default5'] = isset($data['default5']) ? $data['default5'] : 'Ilikeracecarsthatgofast';
		$this->load->view('header');
		$this->load->view('home', $data);
		$this->load->view('footer');
		$this->view->display();
	}
	
	function englishnumbertranslator()
	{
		$data['default1'] = $this->input->request('input1'); 
		try
		{
			$this->load->model('EnglishNumberTranslator', $this->input->request('input1'));
			$result = $this->englishnumbertranslator->getResult();
			$data['output1'] = implode(',', $result);
			$this->index($data);
		}
		catch(Exception $e)
		{
			$data['error1'] = $e->getMessage();
			$this->index($data);
		}
	}
	
	function fibonacci()
	{
		$data['default2'] = $this->input->request('input2'); 
		try
		{
			$this->load->model('Fibonacci', $this->input->request('input2'));
			$result = $this->fibonacci->getResult();
			$data['output2'] = implode(',', $result);
			$this->index($data);
		}
		catch(Exception $e)
		{
			$data['error2'] = $e->getMessage();
			$this->index($data);
		}
	}
	
	function kaprekar()
	{
		$data['default3'] = $this->input->request('input3'); 
		try
		{
			$this->load->model('Kaprekar', $this->input->request('input3'));
			$result = $this->kaprekar->getResult();
			$data['output3'] = $result ? 'true' : 'false';
			$this->index($data);
		}
		catch(Exception $e)
		{
			$data['error3'] = $e->getMessage();
			$this->index($data);
		}
	}
	
	function happy()
	{
		$data['default4'] = $this->input->request('input4'); 
		try
		{
			$this->load->model('Happy', $this->input->request('input4'));
			$result = $this->happy->getResult();
			$data['output4'] = implode(',', $result);
			$this->index($data);
		}
		catch(Exception $e)
		{
			$data['error4'] = $e->getMessage();
			$this->index($data);
		}
	}
	
	function greplin()
	{
		$data['default5'] = $this->input->request('input5'); 
		try
		{
			$this->load->model('Greplin', $this->input->request('input5'));
			$data['output5'] = $this->greplin->getResult();
			$this->index($data);
		}
		catch(Exception $e)
		{
			$data['error5'] = $e->getMessage();
			$this->index($data);
		}
	}
}