<?php

if (!defined('APPPATH'))
{
	define('APPPATH', '../');
}
if (!defined('SYSPATH'))
{
    define('SYSPATH', '../../framework');
}

require_once SYSPATH."/config/bootstrap.php";
$input = new Input();
$controller = new Controller($input->request('controller'), $input->request('method'));
$controller->invoke();
?>
