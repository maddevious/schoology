<?php
$data = isset($data) ? $data : array();
for ($i=1; $i<6; $i++)
{
	$data['output'.$i] = isset($data['output'.$i]) ? $data['output'.$i] : '';
	$data['output'.$i] = isset($data['error'.$i]) ? $data['error'.$i] : $data['output'.$i];
}

$this->form->create('home.php', 'form', 'form', 'post');
$this->form->html('<div class="grouping">');
$this->form->inputElement('method', 'englishnumbertranslator', 'hidden');
$this->form->html('<h1>English-Number Translator</h1>
<div class="instructions">In this problem, you will be given one or more integers in English. Your task is to translate these numbers
into their integer representation. The numbers can range from negative 999,999,999 to positive
999,999,999. The following is an exhaustive list of English words that your program must account for:
negative, zero, one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve, thirteen, fourteen,
fifteen, sixteen, seventeen, eighteen, nineteen, twenty, thirty, forty, fifty, sixty, seventy, eighty, ninety,
hundred, thousand, million</div>
<div>
<span class="title">Input:</span>');
$this->form->inputElement('input1', $data['default1'], 'text', array('class' => 'extralarge'));
$this->form->buttonElement('submit', 'button1', 'Submit');
$errorclass = isset($data['error1']) && $data['error1'] != '' ? 'error' : '';
$this->form->html('<div class="title">Output: <div class="output '.$errorclass.'">'.$data['output1'].'</div></div>
</div>
</div>');
$formhtml = $this->form->getForm();

$this->form->create('home.php', 'form', 'form', 'post');
$this->form->html('<div class="grouping">');
$this->form->inputElement('method', 'fibonacci', 'hidden');
$this->form->html('<h1>Fibanocci Freeze</h1>
<div class="instructions">The Fibonacci numbers (0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, ...) are defined by the recurrence:<br/>
	<em>F</em><sub>0</sub> = 0<br/>
	<em>F</em><sub>1</sub> = 1<br/>
	<em>F</em><sub>i</sub> = <em>F</em><sub>i-1</sub> + <em>F</em><sub>i-2</sub> <em>for all</em> i ≥ 2<br/>
Write a program to calculate the Fibonacci Numbers.</div>
<div>
<span class="title">Input:</span>
');
$this->form->inputElement('input2', $data['default2'], 'text', array('class' => 'large'));
$this->form->buttonElement('submit', 'button2', 'Submit');
$errorclass = isset($data['error2']) && $data['error2'] != '' ? 'error' : '';
$this->form->html('<div class="title">Output: <div class="output '.$errorclass.'">'.$data['output2'].'</div></div>
</div>
</form>
</div>');
$formhtml .= $this->form->getForm();

$this->form->create('home.php', 'form', 'form', 'post');
$this->form->html('<div class="grouping">');
$this->form->inputElement('method', 'kaprekar', 'hidden');
$this->form->html('<h1>Kaprekar Numbers</h1>
<div class="instructions">Consider an n-digit number k. Square it and add the right n digits to the left n
or n-1 digits. If the resultant sum is k, then k is called a Kaprekar number. For
example, 9 is a Kaprekar number since 9<sup>2</sup> = 81 and 8 + 1 = 9 and 297 is a
Kaprekar number since 297<sup>2</sup> = 88209 and 88 + 209 = 297.<br/>
Your task is to write a function that identifies Kaprekar numbers and to determine the Kaprekar numbers
less than a thousand.
</div>
<div>
<span class="title">Input:</span>
');
$this->form->inputElement('input3', $data['default3'], 'text', array('class' => 'small'));
$this->form->buttonElement('submit', 'button3', 'Submit');
$errorclass = isset($data['error3']) && $data['error3'] != '' ? 'error' : '';
$this->form->html('<div class="title">Output: <div class="output '.$errorclass.'">'.$data['output3'].'</div></div>
</div>
</form>
</div>');
$formhtml .= $this->form->getForm();

$this->form->create('home.php', 'form', 'form', 'post');
$this->form->html('<div class="grouping">');
$this->form->inputElement('method', 'happy', 'hidden');
$this->form->html('<h1>Happy Numbers</h1>
<div class="instructions">A happy number is defined by the following process. Starting with any positive integer, replace the
number by the sum of the squares of its digits, and repeat the process until the number equals 1 (where it
will stay), or it loops endlessly in a cycle which does not include 1. Those numbers for which this process
ends in 1 are happy numbers, while those that do not end in 1 are unhappy numbers (or sad numbers).
For example, 7 is a happy number, as 7<sup>2</sup>=49, 4<sup>2</sup>+9<sup>2</sup>=16+81=97, 9<sup>2</sup>+7<sup>2</sup>=81+49=130, 1<sup>2</sup>+3<sup>2</sup>+0<sup>2</sup>=1+9+0=10,
and 1<sup>2</sup>+0<sup>2</sup>=1+0=1. But 17 is not a happy number, as 1<sup>2</sup>+7<sup>2</sup>=1+49=50, 5<sup>2</sup>+0<sup>2</sup>=25+0=25, 2<sup>2</sup>+5<sup>2</sup>=4+25=29,
2<sup>2</sup>+9<sup>2</sup>=4+81=85, 8<sup>2</sup>+5<sup>2</sup>=64+25=89, 8<sup>2</sup>+9<sup>2</sup>=64+81=145, 1<sup>2</sup>+4<sup>2</sup>+5<sup>2</sup>=1+16+25=42, 4<sup>2</sup>+2<sup>2</sup>=16+4=20,
2<sup>2</sup>+0<sup>2</sup>=4+0=4, 4<sup>2</sup>=16, 1<sup>2</sup>+6<sup>2</sup>=1+36=37, 3<sup>2</sup>+7<sup>2</sup>=9+49=58, and 5<sup>2</sup>+8<sup>2</sup>=25+64=89, which forms a loop.
Write a function that takes an array of numbers (comma list) and filters out non-happy numbers (i.e. it returns an array of happy numbers).
</div>
<div>
<span class="title">Input:</span>
');
$this->form->inputElement('input4', $data['default4'], 'text', array('class' => 'large'));
$this->form->buttonElement('submit', 'button4', 'Submit');
$errorclass = isset($data['error4']) && $data['error4'] != '' ? 'error' : '';
$this->form->html('<div class="title">Output: <div class="output '.$errorclass.'">'.$data['output4'].'</div></div>
</div>
</form>
</div>');
$formhtml .= $this->form->getForm();

$this->form->create('home.php', 'form', 'form', 'post');
$this->form->html('<div class="grouping">');
$this->form->inputElement('method', 'greplin', 'hidden');
$this->form->html('<h1>Greplin Challenge</h1>
<div class="instructions">In the block of text below, find the longest substring that is the same in reverse (palindrome).<br/><br/>
As an example, if the input was "Ilikeracecarsthatgofast" the answer would be "racecar".
</div>
<div>
<span class="title">Input:</span>
');
$this->form->textareaElement('input5', $data['default5'], array('class' => 'extralarge'));
$this->form->buttonElement('submit', 'button5', 'Submit');
$errorclass = isset($data['error5']) && $data['error5'] != '' ? 'error' : '';
$this->form->html('<div class="title">Output: <div class="output '.$errorclass.'">'.$data['output5'].'</div></div>
</div>
</form>
</div>');
$formhtml .= $this->form->getForm();

echo $formhtml;
