<?php
require SYSPATH .'/helper/Arrays.php';

class Greplin
{
	private $result;

	function __construct($input)
	{
		if ($input == '')
		{
			throw new Exception('Please enter an input.');
		}
		$input = str_replace(array(' ', "\n"), array('',''), $input);
		$input = strtolower($input);
		$len = strlen($input);
		$len2 = $len -1;
		while ($len > 0)
		{
			for ($i = 0; $i < $len2; $i++)
			{
				$subIn = substr($input, $i, $len);
				if ($this->isPalindrome($subIn))
				{
					$this->result = $subIn;
					return;
				}
			}
			--$len;
		}
	}

	function isPalindrome($a)
	{
		$b = strrev($a);
		return ($b == $a);
	}

	function getResult()
	{
		return $this->result;
	}
}