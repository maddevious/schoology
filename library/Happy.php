<?php
require SYSPATH .'/helper/Arrays.php';

class Happy
{
	private $result = array();
	static $o = array();

	function __construct($input)
	{
		$input = str_replace(' ', '', $input);
		if ($input == '')
		{
			throw new Exception('Please enter an input.');
		}
		$inputs = Arrays::stringToArray($input, ',');
		
		foreach ($inputs as $num)
		{
			self::$o = array();
			if (!is_numeric($num))
			{
				throw new Exception('Please enter a valid number.');
			}
			if ($this->isHappy($num))
			{
				$this->result[] = $num;
			}
		}
	}
	
	function isHappy($num)
	{
		$calc= 0;
		$len = strlen($num);
		$num = "$num";

		for ($i = 0; $i < $len; $i++)
		{
			$calc += $num[$i] * $num[$i];
		}
		if ($calc == 1)
		{
			return true;
		}
		if (in_array($calc, self::$o))
		{
			return false;
		}
		else
		{
			self::$o[] = $calc;
			return $this->isHappy($calc);
		}
	}

	function getResult()
	{
		return $this->result;
	}
}