<?php
require SYSPATH .'/helper/Arrays.php';

class Fibonacci
{
	private $result = array();

	function __construct($input)
	{
		if ($input == '')
		{
			throw new Exception('Please enter an input.');
		}
		$input = str_replace(' ', '', $input);
		$inputs = Arrays::stringToArray($input, ',');
		
		foreach ($inputs as $i)
		{
			if (!is_numeric($i))
			{
				throw new Exception('Please enter a valid number.');
			}
			if ($i > 5000)
			{
				throw new Exception('Please enter a number smaller or equal to 5000.');
			}
			$this->result[] = $this->fibonaccify($i);
		}
	}

	private function fibonaccify($position)
	{
		$o = array();
		for ($i = 0; $i <= $position; $i++)
		{
			if ($i >=2)
			{
				$o[$i] = $o[$i-1] + $o[$i-2];
			}
			else
			{
				$o[$i] = $i;
			}
		}
		return $o[$position];
	}

	function getResult()
	{
		return $this->result;
	}
}