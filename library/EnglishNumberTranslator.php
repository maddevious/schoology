<?php
require SYSPATH .'/helper/Arrays.php';

class EnglishNumberTranslator
{
	private $numLookup = array('negative' => -1, 'zero' => 0, 'one' => 1, 'two' => 2, 'three' => 3, 'four' => 4, 'five' => 5, 'six' => 6, 'seven' => 7, 'eight' => 8,
	'nine' => 9, 'ten' => 10, 'eleven' => 11, 'twelve' => 12, 'thirteen' => 13, 'fourteen' => 14, 'fifteen' => 15, 'sixteen' => 16, 'seventeen' => 17,
	'eighteen' => 18, 'nineteen' => 19, 'twenty' => 20, 'thirty' => 30, 'forty' => 40, 'fifty' => 50, 'sixty' => 60, 'seventy' => 70, 'eighty' => 80,
	'ninety' => 90, 'hundred' => 100, 'thousand' => 1000, 'million' => 1000000);
	private $result = array();

	function __construct($input)
	{
		if ($input == '')
		{
			throw new Exception('Please enter an input.');
		}
		$inputs = Arrays::stringToArray($input, ',');

		foreach ($inputs as $words)
		{
			$output=0;
			$multiplier = 1;
			$words = explode(' ', trim($words));
			foreach (array_reverse($words) as $w)
			{
				$w = trim(strtolower($w));
				$number = isset($this->numLookup[$w]) ? $this->numLookup[$w] : '';
				if ($number == '')
				{
					throw new Exception('Could not translate '.$w.' to a number.');
				}
				if ($number == -1)
				{
					$output *= $number;
				}
				elseif ($number > 90)
				{
					if ($multiplier > $number)
					{
						$multiplier *= $number;
					}
					else
					{
						$multiplier = $number;
					}
				}
				else
				{
					$output += $multiplier * $number;
				}
			}

			$this->result[] = number_format($output);
		}
	}

	function getResult()
	{
		return $this->result;
	}
}