<?php
require SYSPATH .'/helper/Arrays.php';

class Kaprekar
{
	private $result = false;

	function __construct($num)
	{
		$num = str_replace(' ', '', $num);
		if ($num == '')
		{
			throw new Exception('Please enter an input.');
		}
		if (!is_numeric($num))
		{
			throw new Exception('Please enter a valid number.');
		}
		$sq = ($num*$num);
		$len = strlen($sq);
		$rpos = strlen($num);
		$lpos = $len - $rpos;
		
		$l = substr($sq, 0, $lpos);
		$r = substr($sq, "-$rpos");
		$sum = $l + $r;
		if ($sum == $num && $sum < 1000)
		{
			$this->result = true;
		}
		else
		{
			$this->result = false;
		}
	}

	function getResult()
	{
		return $this->result;
	}
}