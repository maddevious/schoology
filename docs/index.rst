PPCare API Specifications
=========================

Authentication to the API is done via an API key and API secret.  The secret must never be shared in publicly accessible areas such as public code repository, client-side code, etc.  Authentication is preformed via HTTP Basic Auth over HTTPS.  Provide your API key as the basic auth username value and the HMAC-SHA256 as the basic auth password. In addition, the nonce and timestamp values must be provided in the header.  The timestamp must be in ISO 8601 UTC format and must be valid within 1 minute of current time.  Endpoints that require sid, a valid sid from auth/login must be provided in the header.

Auth password is a HMAC-SHA256 of your API secret, HTTP method, endpoint, nonce, and ISO 8601 UTC timestamp concatenated by the + character.  This auth password can only be used once.  For example::

    auth_password = base64_encode(hmac("sha256", "api_secret", "POST+api/v1/auth/login+na923ns+20161131T063400Z"))

API endpoints
-------------

  Production: ``https://www.domain.com/api/``

  Test: ``https://qa.domain.com/api/``

Example request
---------------

Curl Request

.. code-block:: bash

  curl https://www.domain.com/api/v1/auth/login \
    -X POST \
    -H nonce=na923ns 
    -H timestamp=20161131T063400Z
    -u d87f7e0c:655d7905db68df5aaa6b67f957770ff8 \
    -d email=john@doe.com \
    -d password=mypassword

more informatin here

.. code-block:: json

  {
    "user": {
        "account_id": "00000000-0000-0000-0000-000000000000",
        "api_key": "00000000-0000-0000-0000-000000000000",
        "status": 1,
        "id": "00000000-0000-0000-0000-000000000000",
        "username": "test_user"
    },
    "result": "success",
    "transaction_id": "00000000-0000-0000-0000-000000000000"
  }

.. note::

  here's a note
  
.. warning::

  here's a warning
  

.. include:: include.rst

